
#### Setup

- **Note:** This repo uses Pipenv but you can use what you want for dependency management.
- **Note:** This instruction set is written for MacOS & the Ansible playbook

1. Ensure you have `Python 3.7` installed
    ```shell script
    ➜ python3 --version
    Python 3.7.7
    ```
1. Install `pipenv`.
    ```
    ➜ brew install pipenv
   ...
    ```

1. Initialize your `pipenv`
    ```shell script
    ➜ pipenv install
    Creating a virtualenv for this project…
    ...
    To activate this project's virtualenv, run pipenv shell.
    Alternatively, run a command inside the virtualenv with pipenv run.
    ```
1. Activate your project's virtualenv
    ```shell script
    ➜ pipenv shell
    Launching subshell in virtual environment…
    . /Users/hulko/.local/share/virtualenvs/gitlab_api-Q0PixHAH/bin/activate
    ```
1. Upgrade your Terraform
   ```
   ➜ ansible-playbook terraform_upgrade.yml
   ```



# Ansible CheatSheet
1. List all Groups
   1. Group Names
      ```
      ➜ ansible localhost -m debug -a 'var=groups.keys()'
      localhost | SUCCESS => {
          "groups.keys()": "dict_keys(['all', 'ungrouped', 'macos'])"
      }
      ```
   1. Include Hosts
      ```
      ➜ ansible localhost -m debug -a 'var=groups'
      localhost | SUCCESS => {
          "groups": {
              "all": [
                  "localhost"
              ],
              "macos": [
                  "localhost"
              ],
              "ungrouped": []
          }
      }
      ```
   1. Using `ansible-inventory`
      ```
      ➜ ansible-inventory -i inventory --list
      {
          "_meta": {
              "hostvars": {
                  "localhost": {
                      "os": "darwin"
                  }
              }
          },
          "all": {
              "children": [
                  "macos",
                  "ungrouped"
              ]
          },
          "macos": {
              "hosts": [
                  "localhost"
              ]
          }
      }
      ```


References
----------
[Ansible Role Directory Structure](https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html#id2)
