# [The Scenario Layout](https://molecule.readthedocs.io/en/2.22/getting-started.html#the-scenario-layout)

---
> A scenario is a self-contained directory containing everything necessary for testing the role in a particular way. The default scenario is named default, and every role should contain a default scenario.
[REF](https://molecule.readthedocs.io/en/2.0.2/configuration.html#molecule.scenario.Scenario)

This default scenario will test a fresh install of base GitLab.
```
$ ls
Dockerfile.j2  INSTALL.rst  molecule.yml  playbook.yml  tests
```

- Since Docker is the default Driver, we find a Dockerfile.j2 Jinja2 template file in place. Molecule will use this file to build a docker image to test your role against.
- *INSTALL.rst* contains instructions on what additional software or setup steps you will need to take in order to allow Molecule to successfully interface with the driver.
- *molecule.yml* is the central configuration entrypoint for Molecule. With this file, you can configure each tool that Molecule will employ when testing your role.
- *playbook.yml* is the playbook file that contains the call site for your role. Molecule will invoke this playbook with ansible-playbook and run it against an instance created by the driver.
- *tests* is the tests directory created because Molecule uses TestInfra as the default Verifier. This allows you to write specific tests against the state of the container after your role has finished executing. Other verifier tools are available.


Idempotence - Runs the converge step a second time. If no tasks will be marked as changed the scenario will be considered
idempotent

_converge_ - convert the state of the instances to the real state declared in the actual roles to be tested
_verify_ - call the test roles

---

The Test Matrix - [ref](https://www.toptechskills.com/ansible-tutorials-courses/rapidly-build-test-ansible-roles-molecule-docker/)
----------------
1. __dependency__ - (optional) download any dependencies from [Ansible Galaxy](https://galaxy.ansible.com/)
1. __lint__ -  run [yamllint](https://github.com/adrienverge/yamllint) and [ansible-lint](https://github.com/ansible/ansible-lint) on YAML files, and [flake8](http://flake8.pycqa.org/en/latest/manpage.html) on the Python test files
1. __destroy__ - make sure that any infrastructure from previous tests is gone
1. __syntax__ - run `ansible-playbook --syntax-check` on the _molecule/default/playbook.yml_ file
1. __create__ - create the instances using the configured driver (docker, ec2, vagrant, etc.)
1. __prepare__ - (optional) run a playbook to prepare the instances after _create_ has finished
1. __converge__ - run _molecule/default/playbook.yml_ on the infrastructure
1. __idempotence__ - run the playbook again to check that nothing is marked as _changed_
1. __side_effect__ - (optional) run a playbook that has side effects on the instance
1. __verify__ - run tests on the instances
1. __cleanup__ - The cleanup playbook is for cleaning up test infrastructure that may not be present on the instance that will be destroyed
1. __destroy__ - tear down the infrastructure and clean up
